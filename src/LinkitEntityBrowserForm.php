<?php

namespace Drupal\linkit_entity_browser;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\entity_browser\Element\EntityBrowserElement;

/**
 * Adds Entity Browser to the Linkit form.
 */
class LinkitEntityBrowserForm {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Linkit profile id.
   *
   * @var string
   */
  protected $linkitProfileId;

  /**
   * The Linkit profile.
   *
   * @var \Drupal\linkit\ProfileInterface
   */
  protected $linkitProfile;

  /**
   * The entity browser to be used with Linkit profile.
   *
   * @var \Drupal\entity_browser\EntityBrowserInterface
   */
  protected $entityBrowser;

  /**
   * LinkitEntityBrowserHelper constructor.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(TranslationInterface $string_translation, EntityTypeManagerInterface $entity_type_manager) {
    $this->stringTranslation = $string_translation;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Alters Linkit profile form to add Entity Browser settings.
   *
   * Allows to choose Entity Browser that should be used by Linkit profile.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param string $form_id
   *   The form id.
   */
  public function settingsFormAlter(array &$form, FormStateInterface $form_state, $form_id) {
    if (empty($form['additional_settings'])) {
      $form['additional_settings'] = [
        '#type' => 'vertical_tabs',
      ];
    }

    $form['linkit_entity_browser'] = [
      '#type' => 'details',
      '#title' => $this->t('Entity Browser'),
      '#group' => 'additional_settings',
      '#tree' => TRUE,
    ];

    $entity_browser_id = $form_state
      ->getFormObject()
      ->getEntity()
      ->getThirdPartySetting('linkit_entity_browser', 'entity_browser_id');

    $entity_browsers = $this
      ->entityTypeManager
      ->getStorage('entity_browser')
      ->loadMultiple();

    $entity_browsers_list = [];
    foreach ($entity_browsers as $entity_browser) {
      $entity_browsers_list[$entity_browser->id()] = $entity_browser->label();
    }

    $form['linkit_entity_browser']['entity_browser_id'] = [
      '#type' => 'select',
      '#default_value' => $entity_browser_id ?? '_none',
      '#options' => [
        '_none' => $this->t('- None -'),
      ] + $entity_browsers_list,
      '#title' => $this->t('Entity browser'),
      '#description' => $this->t('Entity browser that should be used with the Linkit Profile.'),
    ];

    array_unshift(
      $form['actions']['submit']['#submit'],
      [self::class, 'submitSettingsForm'],
    );
  }

  /**
   * Submit handler for Linkit profile settings.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public static function submitSettingsForm(array &$form, FormStateInterface $form_state) {
    $entity_browser_id = $form_state->getValue([
      'linkit_entity_browser',
      'entity_browser_id',
    ]);
    /** @var \Drupal\linkit\Entity\Profile $entity */
    $entity = $form_state->getFormObject()->getEntity();
    if (!$entity_browser_id || $entity_browser_id === '_none') {
      $entity->unsetThirdPartySetting('linkit_entity_browser', 'entity_browser_id');
    }
    else {
      $entity->setThirdPartySetting('linkit_entity_browser', 'entity_browser_id', $entity_browser_id);
    }
  }

  /**
   * Alters Linkit form to add Entity Browser.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function formAlter(array &$form, FormStateInterface $form_state) {
    if (!$this->isApplicableForm($form)) {
      return;
    }

    $persistent_data = $this->getPersistentData();

    $form['entity_browser'] = [
      '#type' => 'entity_browser',
      '#entity_browser' => $this->getEntityBrowser()->id(),
      '#selection_mode' => EntityBrowserElement::SELECTION_MODE_APPEND,
      '#cardinality' => 1,
      '#process' => [
        [EntityBrowserElement::class, 'processEntityBrowser'],
        [$this, 'processEntityBrowser'],
      ],
      '#wrapper_id' => Html::getUniqueId('linkit-entity-browser'),
      '#entity_browser_validators' => $persistent_data['validators'],
      '#widget_context' => $persistent_data['widget_context'],
      '#weight' => 0,
    ];
  }

  /**
   * Process callback for the Entity Browser form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The form element.
   */
  public function processEntityBrowser(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $element['entity_ids']['#ajax'] = [
      'callback' => [static::class, 'insertLink'],
      'wrapper' => $element['#wrapper_id'],
      'event' => 'entity_browser_value_updated',
    ];

    return $element;
  }

  /**
   * Ajax callback to insert selected link in the editor.
   *
   * When user selects entity via Entity Browser, we add it directly to the
   * editor without displaying Linkit modal.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AJAX response object.
   */
  public static function insertLink(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($form_state->hasValue(['entity_browser', 'entities'])) {
      $entity = reset($form_state->getValue(['entity_browser', 'entities']));
      $form_state->setValue('attributes', [
        'href' => "/{$entity->toUrl()->getInternalPath()}",
        'data-entity-type' => $entity->getEntityTypeId(),
        'data-entity-uuid' => $entity->uuid(),
        'data-entity-substitution' => 'canonical',
      ]);
      $form_state->setValue('href_dirty_check', "/{$entity->toUrl()->getInternalPath()}");
      $form_state->setValue('form_build_id', $form['#build_id']);

      $response->addCommand(new EditorDialogSave($form_state->getValues()));
    }
    return $response->addCommand(new CloseDialogCommand());
  }

  /**
   * Check if the form is applicable to be altered.
   *
   * @param array $form
   *   The form.
   *
   * @return bool
   *   TRUE if alteration applies, FALSE otherwise.
   */
  protected function isApplicableForm(array $form) {
    if ($form['attributes']['href']['#type'] !== 'linkit') {
      return FALSE;
    }

    $linkit_profile_id = $form['attributes']['href']['#autocomplete_route_parameters']['linkit_profile'] ?? $form['attributes']['href']['#autocomplete_route_parameters']['linkit_profile_id'];
    if (!$linkit_profile_id) {
      return FALSE;
    }

    $this->setLinkitProfileId($linkit_profile_id);
    if (!$this->getLinkitProfile()) {
      return FALSE;
    }

    if (!$this->getEntityBrowser()) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Sets the Linkit profile id.
   *
   * @param string $linkit_profile_id
   *   The Linkit profile id.
   */
  protected function setLinkitProfileId($linkit_profile_id) {
    $this->linkitProfileId = $linkit_profile_id;
  }

  /**
   * Gets the Linkit profile.
   *
   * @return \Drupal\linkit\ProfileInterface|null
   *   The Linkit profile object. NULL if no matching entity is found.
   */
  protected function getLinkitProfile() {
    if (!$this->linkitProfile && $this->linkitProfileId) {
      $this->linkitProfile = $this
        ->entityTypeManager
        ->getStorage('linkit_profile')
        ->load($this->linkitProfileId);
    }

    return $this->linkitProfile;
  }

  /**
   * Gets the Entity Browser to be used with Linkit profile.
   *
   * @return \Drupal\entity_browser\EntityBrowserInterface|null
   *   The Entity Browser object. NULL if no matching entity is found.
   */
  protected function getEntityBrowser() {
    if ($this->entityBrowser) {
      return $this->entityBrowser;
    }

    $entity_browser_id = $this
      ->getLinkitProfile()
      ->getThirdPartySetting('linkit_entity_browser', 'entity_browser_id');

    if (!$entity_browser_id) {
      return NULL;
    }

    $this->entityBrowser = $this
      ->entityTypeManager
      ->getStorage('entity_browser')
      ->load($entity_browser_id);

    return $this->entityBrowser;
  }

  /**
   * Gets data that should persist across Entity Browser renders.
   *
   * @return array
   *   Data that should persist after the Entity Browser is rendered.
   */
  protected function getPersistentData() {
    $bundles = [];
    foreach ($this->getLinkitProfile()->getMatchers() as $matcher) {
      $target_type = $matcher->getPluginDefinition()['target_entity'];
      $allowed_bundles = $matcher->getConfiguration()['settings']['bundles'];
      $bundles[$target_type] = array_merge(
        $bundles[$target_type] ?? [],
        $allowed_bundles ? array_keys($allowed_bundles) : [$target_type],
      );
    }

    return [
      'validators' => [],
      'widget_context' => [
        'target_bundles' => $bundles,
        'target_entity_types' => array_keys($bundles),
        'cardinality' => 1,
      ],
    ];
  }

}
